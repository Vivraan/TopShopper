﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace co.banzan.TopShopper
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
