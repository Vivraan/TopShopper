﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace co.banzan.TopShopper
{
    public class Item : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField, Range(0f, 1f)] private float fragility;
        [SerializeField] private float itemTransferSpeed = 1f;
        public float Fragility => fragility;

        private Transform originalParent;
        private Vector3 originalPosition;
        public Vector3 OriginalPosition
        {
            set
            {
                originalPosition = value;
            }
        }
        private Vector3 destination;

        private BaggageJumble baggage;
        private AudioSource sfx;
        private float lerpFactor = 1f;
        private bool allowInteraction = true;

        private void Awake()
        {
            baggage = FindObjectOfType<BaggageJumble>();
        }

        private void Start()
        {
            originalParent = transform.parent;
            sfx = GameObject.FindGameObjectWithTag("SFX").GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (0 <= lerpFactor && lerpFactor < 1f)
            {
                if ((transform.position - destination).sqrMagnitude <= Mathf.Epsilon)
                {
                    lerpFactor = 1f;
                    return;
                }

                lerpFactor += Time.deltaTime;
                transform.position = Vector3.Lerp(transform.position, destination, itemTransferSpeed * lerpFactor);
            }
        }

        public void DisallowInteraction()
        {
            allowInteraction = false;
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (allowInteraction)
            {
                lerpFactor = 0f;

                if (transform.parent != baggage.transform)
                {
                    destination = baggage.FindSpot(this);
                    transform.parent = baggage.transform;
                    baggage.Add(this);
                }
                else
                {
                    destination = originalPosition;
                    transform.parent = originalParent;
                    baggage.Remove(this);
                }

                sfx.Play();
            }
        }
    }
}
