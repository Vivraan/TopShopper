﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace co.banzan.TopShopper
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Item[] itemPrefabs;
        [SerializeField] private BoxCollider2D[] shelves;
        [SerializeField] private float itemPadding = 1f;
        [SerializeField] private CanvasScaler canvasScaler;
        [SerializeField] private PlayableAspectHeight[] playableAspectHeights;
        [SerializeField] private TextMeshProUGUI fragileItemLabel;
        [SerializeField] private TextMeshProUGUI countdownLabel;
        [SerializeField] private TextMeshProUGUI levelLabel;
        [SerializeField] private Image startPanel;
        [SerializeField] private Image endPanel;
        [SerializeField] private BaggageJumble baggage;
        [SerializeField] private VerticalLayoutGroup checklistPanel;
        [SerializeField] private TextMeshProUGUI checklistEntryPrefab;
        [SerializeField] private float defeatCountdownTime = 5f;
        [SerializeField] private int maxLevel = 2;
        [SerializeField] private int numCompulsaryStrongItems = 2;

        private float refResolutionWidth = 720f;
        private bool firstTime = true;
        private float countdown;
        private Coroutine defeatCountdown;
        private int shelvesFilled = 0;
        private int level = 1;
        private List<Item> itemsInPlay = new List<Item>();
        private Dictionary<Item, TextMeshProUGUI> checklistEntryLookup = new Dictionary<Item, TextMeshProUGUI>();

        public static void SortItemsByFragility(ref List<Item> items, bool reverse = false)
        {
            int direction = reverse ? +1 : -1;
            items.Sort((l, r) => (int)(direction * (l.Fragility - r.Fragility) * 100));
        }

        private void Awake()
        {
            var aspectRatio = (float)Screen.height / Screen.width;
            var refHeight = 1280f;

            foreach (var pair in playableAspectHeights)
            {
                if (Mathf.Abs(pair.aspectRatio - aspectRatio) < 0.01f)
                {
                    refHeight = pair.referenceHeight;
                    break;
                }
            }

            canvasScaler.referenceResolution = new Vector2(refHeight, refResolutionWidth);
        }

        private void Update()
        {
            if (firstTime && Input.GetMouseButton(0))
            {
                firstTime = false;
                foreach (Transform child in startPanel.transform)
                {
                    child.gameObject.SetActive(false);
                }

                startPanel.GetComponent<Fader>().StartFade();
                InitGame(level);
                startPanel.gameObject.SetActive(false);
            }
        }

        public void InitGame(int level)
        {
            Setup();
            GenerateItems();
            ChooseItemsToPick();

            baggage.SetupOrderedItems(checklistEntryLookup.Keys);

            void Setup()
            {
                levelLabel.text = $"Level {level}";
                endPanel.gameObject.SetActive(false);
                countdownLabel.gameObject.SetActive(false);
                fragileItemLabel.gameObject.SetActive(false);

                foreach (var item in itemsInPlay)
                {
                    Destroy(item.gameObject);
                }

                if (checklistEntryLookup.Count > 0)
                {
                    foreach (var item in checklistEntryLookup.Keys)
                    {
                        Destroy(checklistEntryLookup[item]);
                    }
                }

                itemsInPlay.Clear();
                baggage.Clear();
                checklistEntryLookup.Clear();
            }

            void GenerateItems()
            {
                var prefabsToSpawn = new List<Item>(itemPrefabs);

                for (shelvesFilled = 0; prefabsToSpawn.Count > 0 && shelvesFilled < shelves.Length; shelvesFilled++)
                {
                    var shelf = shelves[shelvesFilled];
                    var leftCorner = shelf.bounds.min;
                    leftCorner.x += itemPadding;
                    var rightCornerX = leftCorner.x;

                    while (prefabsToSpawn.Count > 0 && rightCornerX < shelf.bounds.max.x)
                    {
                        // Pick an item at random and add it to items in play, and remove its prefab
                        var pickedItemPrefab = prefabsToSpawn[Random.Range(0, prefabsToSpawn.Count)];
                        var item = Instantiate(pickedItemPrefab, shelf.transform);
                        itemsInPlay.Add(item);
                        prefabsToSpawn.Remove(pickedItemPrefab);

                        var itemCollider = item.GetComponent<BoxCollider2D>();

                        var spawnPoint = leftCorner + itemCollider.bounds.extents;
                        rightCornerX = leftCorner.x + itemCollider.bounds.size.x + itemPadding;

                        // If the right corner is within bounds, place the instantiated item at the spawn point
                        if (rightCornerX < shelf.bounds.max.x)
                        {
                            itemCollider.transform.position = spawnPoint;
                            item.OriginalPosition = spawnPoint;
                            leftCorner.x = rightCornerX;
                        }
                        else // remove the item from play, destroy it, and reinstate its prefab
                        {
                            itemsInPlay.Remove(item);
                            Destroy(item.gameObject);
                            prefabsToSpawn.Add(pickedItemPrefab);
                        }
                    }
                }
            }

            void ChooseItemsToPick()
            {
                var itemsToConsider = new List<Item>(itemsInPlay);
                SortItemsByFragility(ref itemsToConsider, reverse: true);

                var itemsToPick = itemsToConsider
                    .FindAll(item => item.Fragility == 1f)
                    .OrderBy(_ => new System.Random().Next())
                    .Take(numCompulsaryStrongItems).ToList();

                itemsToConsider = itemsToConsider.Except(itemsToPick).ToList();

                itemsToPick.AddRange(itemsToConsider.Take(level + 2));
                itemsToPick.Shuffle();

                foreach (var item in itemsToPick)
                {
                    var checklistEntry = Instantiate(checklistEntryPrefab, checklistPanel.transform);
                    checklistEntry.text = item.name.Replace("(Clone)", string.Empty);
                    checklistEntry.fontStyle = FontStyles.Normal;
                    checklistEntryLookup.Add(item, checklistEntry);
                }
            }
        }

        public void ItemAdded(Item item)
        {
            ToggleCheckoutItem(item);
        }

        public void ItemRemoved(Item item)
        {
            ToggleCheckoutItem(item);
        }

        private void ToggleCheckoutItem(in Item item)
        {
            if (checklistEntryLookup.ContainsKey(item))
            {
                checklistEntryLookup[item].fontStyle =
                    checklistEntryLookup[item].fontStyle == FontStyles.Normal ? FontStyles.Strikethrough : FontStyles.Normal;
            }
        }

        public void Victory()
        {
            DisableItems();
            endPanel.gameObject.SetActive(true);
            var animator = endPanel.GetComponentInChildren<Animator>();
            var fader = endPanel.GetComponent<Fader>();
            fader.onFadeEnded += () => animator.enabled = true;
            fader.StartFade();
            StartCoroutine(CountdownToNextLevel());

            IEnumerator CountdownToNextLevel()
            {
                yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
                yield return new WaitUntil(() => Input.GetMouseButton(0));

                if (level < maxLevel)
                {
                    InitGame(++level);
                }
                else
                {
                    endPanel.GetComponentInChildren<TextMeshProUGUI>().text = "Thanks for playing!";
                }
            }
        }

        public void FragileItemFound()
        {
            countdownLabel.gameObject.SetActive(true);
            fragileItemLabel.gameObject.SetActive(true);

            countdown = defeatCountdownTime;

            if (defeatCountdown == null)
            {
                defeatCountdown = StartCoroutine(CountdownToDefeat());
            }

            IEnumerator CountdownToDefeat()
            {
                while (countdown >= 0f)
                {
                    countdownLabel.text = countdown.ToString();
                    countdown--;
                    yield return new WaitForSeconds(1f);
                }
                if (countdown < 0f)
                {
                    GameOver();
                    yield return new WaitUntil(() => Input.GetMouseButton(0));
                    yield return SceneManager.LoadSceneAsync(0);
                }
            }

            void GameOver()
            {
                DisableItems();
                countdownLabel.text = "Game Over";
                countdownLabel.gameObject.SetActive(false);
                endPanel.gameObject.SetActive(true);
                endPanel.transform.GetChild(0).gameObject.SetActive(false);
                var fader = endPanel.GetComponent<Fader>();
                fader.onFadeEnded = () => countdownLabel.gameObject.SetActive(true);
                fader.StartFade();
            }
        }

        private void DisableItems()
        {
            foreach (var item in itemsInPlay)
            {
                item.DisallowInteraction();
            }
        }

        public void FragileItemRemoved()
        {
            if (defeatCountdown != null)
            {
                StopCoroutine(defeatCountdown);
            }
            defeatCountdown = null;
            FragileItemFound();
        }

        public void LastFragileItemRemoved()
        {
            if (defeatCountdown != null)
            {
                StopCoroutine(defeatCountdown);
            }
            defeatCountdown = null;
            countdownLabel.gameObject.SetActive(false);
            fragileItemLabel.gameObject.SetActive(false);
        }
    }

    [System.Serializable]
    public class PlayableAspectHeight
    {
        public float aspectRatio;
        public float referenceHeight;
    }

    public static class ListExtensions
    {
        private static System.Random rng = new System.Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
