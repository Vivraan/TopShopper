﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace co.banzan.TopShopper
{
    public class BaggageJumble : MonoBehaviour
    {
        [System.Serializable]
        public class UnityEventItem : UnityEvent<Item> { }

        [SerializeField] private UnityEventItem onItemAdded;
        [SerializeField] private UnityEventItem onItemRemoved;
        [SerializeField] private UnityEvent onFragileItemAdded;
        [SerializeField] private UnityEvent onFragileItemRemoved;
        [SerializeField] private UnityEvent onLastFragileItemRemoved;
        [SerializeField] private UnityEvent onWin;
        private List<Item> items = new List<Item>();
        private List<Item> orderedItems;
        private readonly ItemComparer itemComparer = new ItemComparer();

        private BoxCollider2D boxCollider;

        private void Awake()
        {
            boxCollider = GetComponent<BoxCollider2D>();
        }

        public void SetupOrderedItems(in IEnumerable<Item> itemsToPick)
        {
            orderedItems = new List<Item>(itemsToPick);
            GameManager.SortItemsByFragility(ref orderedItems);
        }

        public void Clear()
        {
            items.Clear();
        }

        public void Add(in Item item)
        {
            items.Add(item);
            onItemAdded.Invoke(item);

            if (!orderedItems.Take(items.Count).SequenceEqual(items, itemComparer))
            {
                onFragileItemAdded.Invoke();
            }
            else if (orderedItems.Count == items.Count && orderedItems.SequenceEqual(items, itemComparer))
            {
                onWin.Invoke();
            }
        }

        public void Remove(in Item item)
        {
            items.Remove(item);
            onItemRemoved.Invoke(item);

            if (!orderedItems.Take(items.Count).SequenceEqual(items, itemComparer))
            {
                onFragileItemRemoved.Invoke();
            }
            else
            {
                onLastFragileItemRemoved.Invoke();
            }
        }

        public Vector3 FindSpot(in Item item)
        {
            var bagBounds = boxCollider.bounds;
            var itemBounds = item.GetComponent<BoxCollider2D>().bounds;

            return new Vector3
            (
                Random.Range(bagBounds.min.x + itemBounds.extents.x, bagBounds.max.x - itemBounds.extents.x),
                Random.Range(bagBounds.min.y + itemBounds.extents.y, bagBounds.max.y - itemBounds.extents.y),
                0f
            );
        }

        // Custom comparer for the Item class
        class ItemComparer : IEqualityComparer<Item>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(Item x, Item y)
            {
                //Check whether the compared objects reference the same data.
                if (ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (x is null || y is null)
                    return false;

                //Check whether the items' properties are equal.
                return x.Fragility == y.Fragility;
            }

            // If Equals() returns true for a pair of objects
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(Item item)
            {
                //Check whether the object is null
                if (item is null) return 0;

                return item.Fragility.GetHashCode();
            }
        }

    }
}
